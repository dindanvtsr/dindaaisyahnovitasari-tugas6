import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import Splashscreen from './screen/Splashscreen';
import AuthNavigation from './AuthNavigation';
import BottomNav from './screen/BottomNav';
import HomeNavigation from './HomeNavigation';
import ProfileNavigation from './ProfileNavigation';

const Stack = createNativeStackNavigator();

export default function Routing() {
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{headerShown: false}}>
        <Stack.Screen name="SplashScreen" component={Splashscreen} />
        <Stack.Screen name="AuthNavigation" component={AuthNavigation} />
        <Stack.Screen name="BottomNav" component={BottomNav} />
        <Stack.Screen name="HomeNavigation" component={HomeNavigation} />
        <Stack.Screen name="ProfileNavigation" component={ProfileNavigation} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
