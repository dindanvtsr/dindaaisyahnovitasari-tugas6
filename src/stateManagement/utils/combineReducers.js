import {combineReducers} from 'redux';
import homeReducers from './reducers/homeReducers';
import cartReducers from './reducers/cartReducers';

const rootReducer = combineReducers({
  data: homeReducers,
  dataCart: cartReducers,
});

export default rootReducer;
