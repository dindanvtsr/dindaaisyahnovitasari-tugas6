const initialState = {
  toko: [],
};

const homeReducers = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD_DATA':
      return {
        ...state,
        toko: action.data,
      };

    default:
      return state;
  }
};

export default homeReducers;
