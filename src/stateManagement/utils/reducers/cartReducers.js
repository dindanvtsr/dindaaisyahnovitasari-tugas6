import {SET_CHECKBOX_VALUE} from '../actions/cartActions';

const initialState = {
  cart: [],
};

const cartReducers = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD_CART_DATA':
      return {
        ...state,
        cart: action.data,
      };
    default:
      return state;
  }
};

export default cartReducers;
