import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import Homepage from './screen/Homepage';
import Detail from './screen/Detail';
import FormPemesanan from './screen/FormPemesanan';
import Keranjang from './screen/Keranjang';
import DetailKeranjang from './screen/DetailKeranjang';
import KodeReservasi from './screen/KodeReservasi';
import Checkout from './screen/Checkout';
import AddDataHome from './screen/AddDataHome';

const Stack = createNativeStackNavigator();

export default function HomeNavigation() {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="Homepage" component={Homepage} />
      <Stack.Screen name="Detail" component={Detail} />
      <Stack.Screen
        name="FormPemesanan"
        component={FormPemesanan}
        options={{headerShown: true, headerTitle: 'Formulir Pemesanan'}}
      />
      <Stack.Screen
        name="Keranjang"
        component={Keranjang}
        options={{headerShown: true, headerTitle: 'Keranjang'}}
      />
      <Stack.Screen
        name="DetailKeranjang"
        component={DetailKeranjang}
        options={{headerShown: true, headerTitle: 'Summary'}}
      />
      <Stack.Screen
        name="KodeReservasi"
        component={KodeReservasi}
        options={{headerShown: true, headerTitle: ' '}}
      />
      <Stack.Screen
        name="Checkout"
        component={Checkout}
        options={{headerShown: true, headerTitle: 'Checkout'}}
      />
      <Stack.Screen
        name="AddDataHome"
        component={AddDataHome}
        options={{headerShown: true, headerTitle: 'Add Store Data'}}
      />
    </Stack.Navigator>
  );
}
