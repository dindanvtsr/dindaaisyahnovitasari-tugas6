import React, {useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  StyleSheet,
  ScrollView,
  KeyboardAvoidingView,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {useDispatch, useSelector} from 'react-redux';

const AddDataHome = ({navigation, route}) => {
  const [namaToko, setNamaToko] = useState('');
  const [alamat, setAlamat] = useState('');
  const [jamBuka, setJamBuka] = useState('');
  const [jamTutup, setJamTutup] = useState('');
  const [rating, setRating] = useState('');
  const [minimal, setMinimal] = useState('');
  const [maksimal, setMaksimal] = useState('');
  const [deskripsi, setDeskripsi] = useState('');
  const [gambarToko, setGambarToko] = useState('');
  const [buttonSelected, setButtonSelected] = useState(null);

  const {toko} = useSelector(state => state.data);
  const dispatch = useDispatch();

  const handleOptionSelect = option => {
    setButtonSelected(option);
  };

  const storeData = () => {
    var date = new Date();
    var dataHome = [...toko];
    const data = {
      idStore: date.getMilliseconds(),
      address: alamat,
      openTime: jamBuka,
      closeTime: jamTutup,
      description: deskripsi,
      // favourite: false,
      isOpen: buttonSelected === 'buka',
      minimumPrice: minimal,
      maximumPrice: maksimal,
      ratings: rating,
      storeImage: gambarToko,
      storeName: namaToko,
    };
    dataHome.push(data);
    dispatch({type: 'ADD_DATA', data: dataHome});
    navigation.goBack();
  };

  return (
    <View style={styles.all}>
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 50}}>
        <KeyboardAvoidingView
          behavior="padding"
          enabled
          keyboardVerticalOffset={-500}>
          <View style={styles.isiForm}>
            <Text style={styles.label}>Nama Toko</Text>
            <TextInput
              style={styles.input}
              value={namaToko}
              onChangeText={text => setNamaToko(text)}
              placeholder="Masukkan Nama Toko"
            />
            <Text style={styles.label}>Alamat</Text>
            <TextInput
              style={styles.input}
              value={alamat}
              onChangeText={text => setAlamat(text)}
              placeholder="Masukkan Nama Toko"
            />
            <Text style={styles.label}>Status Buka Tutup</Text>
            <View style={{flexDirection: 'row', marginTop: 11}}>
              <TouchableOpacity onPress={() => handleOptionSelect('buka')}>
                <View style={{flexDirection: 'row'}}>
                  <Icon
                    name={
                      buttonSelected === 'buka'
                        ? 'checkmark-circle'
                        : 'radio-button-off'
                    }
                    color="#BB2427"
                    size={23}
                  />
                  <Text style={styles.bukaTutup}>Buka</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => handleOptionSelect('tutup')}>
                <View style={{flexDirection: 'row'}}>
                  <Icon
                    name={
                      buttonSelected === 'tutup'
                        ? 'checkmark-circle'
                        : 'radio-button-off'
                    }
                    color="#BB2427"
                    size={23}
                  />
                  <Text style={styles.bukaTutup}>Tutup</Text>
                </View>
              </TouchableOpacity>
            </View>

            <View style={{flexDirection: 'row'}}>
              <View style={{width: '47%'}}>
                <Text style={styles.label}>Jam Buka</Text>
                <TextInput
                  style={styles.input}
                  value={jamBuka}
                  onChangeText={text => setJamBuka(text)}
                />
              </View>
              <View style={{width: '5%'}}></View>
              <View style={{width: '47%'}}>
                <Text style={styles.label}>Jam Tutup</Text>
                <TextInput
                  style={styles.input}
                  value={jamTutup}
                  onChangeText={text => setJamTutup(text)}
                />
              </View>
            </View>

            <Text style={styles.label}>Jumlah Rating</Text>
            <TextInput
              style={styles.input}
              value={rating}
              onChangeText={text => setRating(text)}
              placeholder="Masukkan Jumlah Rating"
            />
            <View style={{flexDirection: 'row'}}>
              <View style={{width: '47%'}}>
                <Text style={styles.label}>Harga Minimal</Text>
                <TextInput
                  style={styles.input}
                  value={minimal}
                  onChangeText={text => setMinimal(text)}
                  keyboardType="number-pad"
                />
              </View>
              <View style={{width: '5%'}}></View>
              <View style={{width: '47%'}}>
                <Text style={styles.label}>Harga Maksimal</Text>
                <TextInput
                  style={styles.input}
                  value={maksimal}
                  onChangeText={text => setMaksimal(text)}
                  keyboardType="number-pad"
                />
              </View>
            </View>
            <Text style={styles.label}>Deskripsi</Text>
            <TextInput
              style={styles.input}
              value={deskripsi}
              onChangeText={text => setDeskripsi(text)}
              placeholder="Masukkan Deskripsi"
              multiline
            />
            <Text style={styles.label}>Gambar Toko</Text>
            <TextInput
              style={styles.input}
              value={gambarToko}
              onChangeText={text => setGambarToko(text)}
              placeholder="Masukkan Link Gambar"
            />
            <TouchableOpacity
              style={styles.masukkanButton}
              onPress={() => {
                storeData();
              }}>
              <Text
                style={{
                  color: '#fff',
                  fontSize: 16.3,
                  fontWeight: 'bold',
                }}>
                Simpan
              </Text>
            </TouchableOpacity>
          </View>
        </KeyboardAvoidingView>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  all: {
    backgroundColor: '#fff',
    flex: 1,
  },
  isiForm: {
    marginHorizontal: 20,
  },
  label: {
    color: '#BB2427',
    fontWeight: '600',
    fontSize: 12,
    marginTop: 25,
  },
  input: {
    backgroundColor: '#F6F8FF',
    borderRadius: 7.24,
    marginTop: 11,
    paddingLeft: 9,
  },
  masukkanButton: {
    width: '100%',
    marginTop: 35,
    backgroundColor: '#BB2427',
    borderRadius: 8,
    paddingVertical: 15,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 35,
  },
  checkboxLabel: {
    flexDirection: 'row',
  },
  checkboxLabelText: {
    marginTop: 7,
    color: '#201F26',
    fontSize: 14,
    marginLeft: 10,
  },
  checkboxContainer: {
    marginTop: 33,
    marginBottom: 20,
  },
  bukaTutup: {
    color: 'black',
    fontSize: 12,
    fontWeight: '600',
    marginTop: 3,
    marginLeft: 5,
    marginRight: 30,
  },
});

export default AddDataHome;
