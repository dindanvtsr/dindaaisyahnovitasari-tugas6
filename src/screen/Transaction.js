import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  ScrollView,
} from 'react-native';

const Transaction = ({navigation}) => {
  return (
    <View style={styles.all}>
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 50}}>
        <View style={styles.listTransaksi}>
          <TouchableOpacity
            style={styles.kotakSectionProduk}
            onPress={() =>
              navigation.navigate('HomeNavigation', {screen: 'KodeReservasi'})
            }>
            <View
              style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <View style={styles.isiProduk}>
                <View style={{flexDirection: 'row'}}>
                  <Text style={{color: '#BDBDBD', fontSize: 12}}>
                    20 Desember 2020
                  </Text>
                  <Text
                    style={{marginLeft: 11, color: '#BDBDBD', fontSize: 12}}>
                    09:00
                  </Text>
                </View>
                <Text style={styles.judulProduk}>
                  New Balance - Pink Abu - 40
                </Text>
                <Text style={styles.jenis}>Cuci Sepatu</Text>
                <View
                  style={{
                    flexDirection: 'row',
                  }}>
                  <View>
                    <Text style={styles.kodeReservasi}>Kode Reservasi :</Text>
                  </View>
                  <View style={{width: '52%', marginLeft: 6}}>
                    <Text style={styles.isiKode}>CS201201</Text>
                  </View>
                  <View>
                    <Text style={styles.reserved}>Reserved</Text>
                  </View>
                </View>
              </View>
            </View>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  all: {
    backgroundColor: '#F6F8FF',
    flex: 1,
  },
  back: {
    width: 24,
    height: 24,
    marginLeft: 21,
    marginTop: 19,
  },
  headerTitle: {
    color: '#201F26',
    fontWeight: 'bold',
    fontSize: 18,
    marginLeft: 10,
    marginTop: 17,
    marginBottom: 22,
  },
  listTransaksi: {
    marginHorizontal: 20,
  },
  kotakSectionProduk: {
    backgroundColor: '#fff',
    borderRadius: 9,
    marginBottom: 5,
    marginTop: 9,
    elevation: 3,
    paddingVertical: 18,
    paddingLeft: 10,
  },
  judulProduk: {
    fontWeight: '500',
    color: '#201F26',
    fontSize: 12,
    marginTop: 13,
  },
  jenis: {
    color: '#201F26',
    fontSize: 12,
    fontWeight: '400',
    marginBottom: 13,
    marginTop: 3,
  },
  kodeReservasi: {
    color: '#201F26',
    fontSize: 12,
    fontWeight: '400',
  },
  isiKode: {
    color: '#201F26',
    fontSize: 12,
    fontWeight: '700',
  },
  reserved: {
    fontWeight: '400',
    color: '#FFC107',
    backgroundColor: '#f29c1f29',
    paddingHorizontal: 10,
    paddingVertical: 2,
    fontSize: 12,
    borderRadius: 10.5,
  },
});

export default Transaction;
