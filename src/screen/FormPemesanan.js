import React, {useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  TextInput,
  ScrollView,
  KeyboardAvoidingView,
  FlatList,
} from 'react-native';
// import CheckBox from '@react-native-community/checkbox';
import {useDispatch, useSelector} from 'react-redux';
import Icon from 'react-native-vector-icons/Ionicons';
import Icon1 from 'react-native-vector-icons/AntDesign';

const FormPemesanan = ({navigation}) => {
  const [merk, setMerk] = useState('');
  const [warna, setWarna] = useState('');
  const [ukuran, setUkuran] = useState('');
  const [gambar, setGambar] = useState('');
  const [catatan, setCatatan] = useState('');
  const [checkbox, setCheckbox] = useState([
    {key: 'Ganti Sol Sepatu', checked: false},
    {key: 'Jahit Sepatu', checked: false},
    {key: 'Repaint Sepatu', checked: false},
    {key: 'Cuci Sepatu', checked: false},
  ]);

  const {cart} = useSelector(state => state.dataCart);
  const dispatch = useDispatch();

  const handleCheckboxPress = item => {
    const updatedData = checkbox.map(x =>
      x.key === item.key ? {...x, checked: !x.checked} : x,
    );
    setCheckbox(updatedData);
  };

  const storeData = () => {
    var date = new Date();
    var dataCart1 = [...cart];
    const data = {
      idStore: date.getMilliseconds(),
      merk: merk,
      color: warna,
      size: ukuran,
      imageProduct: gambar,
      note: catatan,
    };
    dataCart1.push(data);
    dispatch({type: 'ADD_CART_DATA', data: dataCart1});
    navigation.navigate('HomeNavigation', {
      screen: 'Keranjang',
      // params: {item},
    });
  };

  return (
    <View style={styles.all}>
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 50}}>
        <KeyboardAvoidingView
          behavior="padding"
          enabled
          keyboardVerticalOffset={-500}>
          <View style={styles.isiForm}>
            <Text style={styles.label}>Merek</Text>
            <TextInput
              style={styles.input}
              onChangeText={text => setMerk(text)}
              value={merk}
              placeholder="Masukkan Merk Barang"
            />
            <Text style={styles.label}>Warna</Text>
            <TextInput
              style={styles.input}
              onChangeText={text => setWarna(text)}
              value={warna}
              placeholder="Warna Barang, cth : Merah - Putih "
            />
            <Text style={styles.label}>Ukuran</Text>
            <TextInput
              style={styles.input}
              onChangeText={text => setUkuran(text)}
              value={ukuran}
              placeholder="Cth : S, M, L / 39,40"
            />
            <Text style={styles.label}>Photo</Text>
            <TextInput
              style={styles.input}
              onChangeText={text => setGambar(text)}
              value={gambar}
              placeholder="Masukkan Link"
            />
            {/* <View style={styles.cameraContainer}>
              <Image
                source={require('../assets/icon/camera.png')}
                style={styles.camera}></Image>
              <Text style={styles.addPhoto}>Add Photo</Text>
            </View> */}
            <View style={{marginBottom: 20}}></View>
            <FlatList
              data={checkbox}
              renderItem={({item}) => (
                <TouchableOpacity onPress={() => handleCheckboxPress(item)}>
                  <View style={styles.checkboxContainer}>
                    <View style={styles.checkboxLabel}>
                      {item.checked ? (
                        <Icon
                          name="square-outline"
                          size={25}
                          style={{color: '#B8B8B8'}}
                        />
                      ) : (
                        <Icon1
                          name="checksquare"
                          size={25}
                          style={{color: '#BB2427'}}
                        />
                      )}
                      <Text style={styles.checkboxLabelText}>{item.key}</Text>
                    </View>
                  </View>
                </TouchableOpacity>
              )}
            />
            <View style={{marginBottom: 20}}></View>
            <Text style={styles.label}>Catatan</Text>
            <TextInput
              style={styles.inputCatatan}
              onChangeText={text => setCatatan(text)}
              value={catatan}
              placeholder="Cth : ingin ganti sol baru"
            />
            <TouchableOpacity
              style={styles.masukkanButton}
              onPress={() =>
                navigation.navigate('HomeNavigation', {screen: 'Keranjang'})
              }>
              <Text
                style={{
                  color: '#fff',
                  fontSize: 16.3,
                  fontWeight: 'bold',
                }}
                onPress={() => {
                  storeData();
                }}>
                Masukkan Keranjang
              </Text>
            </TouchableOpacity>
          </View>
        </KeyboardAvoidingView>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  all: {
    backgroundColor: '#fff',
    flex: 1,
  },
  back: {
    width: 24,
    height: 24,
    marginLeft: 21,
    marginTop: 19,
  },
  headerTitle: {
    color: '#201F26',
    fontWeight: 'bold',
    fontSize: 18,
    marginLeft: 10,
    marginTop: 17,
    marginBottom: 22,
  },
  isiForm: {
    marginHorizontal: 20,
  },
  label: {
    color: '#BB2427',
    fontWeight: '600',
    fontSize: 12,
    marginTop: 25,
  },
  input: {
    backgroundColor: '#F6F8FF',
    borderRadius: 7.24,
    marginTop: 11,
    paddingLeft: 9,
  },
  camera: {
    width: 20,
    height: 18,
  },
  cameraContainer: {
    width: '25%',
    height: '10%',
    borderRadius: 8,
    borderWidth: 1,
    borderColor: '#BB2427',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 11,
  },
  masukkanButton: {
    width: '100%',
    marginTop: 35,
    backgroundColor: '#BB2427',
    borderRadius: 8,
    paddingVertical: 15,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 35,
  },
  addPhoto: {
    color: '#BB2427',
    fontSize: 12,
    marginTop: 10,
  },
  inputCatatan: {
    backgroundColor: '#F6F8FF',
    borderRadius: 7.24,
    marginTop: 11,
    paddingLeft: 9,
    paddingTop: 14,
    paddingBottom: 63,
  },
  checkboxLabel: {
    flexDirection: 'row',
  },
  checkboxLabelText: {
    marginTop: 3,
    color: '#201F26',
    fontSize: 14,
    marginLeft: 10,
  },
  checkboxContainer: {
    marginTop: 20,
  },
});

export default FormPemesanan;
