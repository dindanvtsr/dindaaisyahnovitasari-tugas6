import React, {useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  ScrollView,
  FlatList,
} from 'react-native';

const Item = ({name, img}) => {
  return (
    <View style={styles.item}>
      <Image
        source={img}
        style={{width: 70, height: 50, resizeMode: 'contain'}}></Image>
      <Text
        style={{
          color: '#000',
          fontSize: 12,
          fontWeight: '400',
        }}>
        {name}
      </Text>
    </View>
  );
};

const Checkout = ({navigation}) => {
  const jenisPembayaran = [
    {
      id: '1',
      name: 'Bank Transfer',
      img: require('../assets/image/uang.png'),
    },
    {
      id: '2',
      name: 'OVO',
      img: require('../assets/image/ovo.png'),
    },
    {
      id: '3',
      name: 'Kartu Kredit',
      img: require('../assets/image/card.png'),
    },
  ];

  const renderItem = ({item}) => <Item name={item.name} img={item.img} />;

  return (
    <View style={styles.all}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.listSummary}>
          <View style={styles.kotakSummary}>
            <Text style={styles.dataTitle}>Data Customer</Text>
            <View style={{flexDirection: 'row'}}>
              <Text style={styles.isiData}>Agil Bani</Text>
              <Text style={styles.isiData1}>(0813763476)</Text>
            </View>
            <Text style={styles.isiData}>
              Jl. Perumnas, Condong catur, Sleman, Yogyakarta
            </Text>
            <Text style={styles.isiData}>gantengdoang@dipanggang.com</Text>
          </View>
          <View style={styles.kotakSummary}>
            <Text style={styles.dataTitle}>Alamat Outlet Tujuan</Text>
            <View style={{flexDirection: 'row'}}>
              <Text style={styles.isiData}>Jack Repair - Seturan</Text>
              <Text style={styles.isiData1}>(027-343457)</Text>
            </View>
            <Text style={styles.isiData}>
              Jl. Affandi No 18, Sleman, Yogyakarta
            </Text>
          </View>
          <View style={styles.kotakSummary}>
            <Text style={styles.dataTitle}>Barang</Text>
            <View
              style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <Image
                source={require('../assets/image/produk.png')}
                style={styles.imageProduk}
              />
              <View style={styles.isiProduk}>
                <Text style={{fontWeight: '500', color: '#000'}}>
                  New Balance - Pink Abu - 40
                </Text>
                <Text style={{color: '#737373'}}>Cuci Sepatu</Text>
                <Text style={{color: '#737373'}}>Note : -</Text>
              </View>
            </View>
            <View
              style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <Text style={styles.pasang}>1 Pasang</Text>
              <Text style={styles.hargaPasang}>@Rp 50.000</Text>
            </View>
          </View>
          <View style={styles.kotakSummary}>
            <Text style={styles.dataTitle}>Rincian Pembayaran</Text>
            <View style={styles.isiRincian}>
              <View style={{flexDirection: 'row'}}>
                <Text style={styles.jenisLayanan}>Cuci Sepatu</Text>
                <Text style={styles.jumlah}>x1 Pasang</Text>
              </View>
              <Text style={styles.hargaLayanan}>Rp. 30.000</Text>
            </View>
            <View style={styles.isiRincian}>
              <View style={{flexDirection: 'row'}}>
                <Text style={styles.jenisLayanan}>Biaya Antar</Text>
              </View>
              <Text style={styles.hargaLayanan}>Rp. 3000</Text>
            </View>
            <View style={styles.lineStyle} />
            <View style={styles.isiRincian}>
              <View style={{flexDirection: 'row'}}>
                <Text style={styles.jenisLayanan}>Total</Text>
              </View>
              <Text style={styles.totalLayanan}>Rp. 33.000</Text>
            </View>
          </View>
          <View style={styles.kotakSummary}>
            <Text style={styles.dataTitle}>Pilih Pembayaran</Text>
            <FlatList
              horizontal
              data={jenisPembayaran}
              renderItem={renderItem}
              keyExtractor={item => item.id}
              showsHorizontalScrollIndicator={false}
            />
          </View>
          <View style={styles.kotakButtonPesan}>
            <View style={styles.reservasiContainer}>
              <TouchableOpacity
                style={styles.reservasiButton}
                onPress={() => navigation.navigate('Transaction')}>
                <Text
                  style={{
                    color: '#fff',
                    fontSize: 16.3,
                    fontWeight: 'bold',
                  }}>
                  Pesan Sekarang
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  all: {
    backgroundColor: '#F6F8FF',
    flex: 1,
  },
  kotakSummary: {
    backgroundColor: '#fff',
    marginTop: 3,
    paddingVertical: 18,
    paddingLeft: 26,
    marginBottom: 12,
  },
  dataTitle: {
    fontWeight: '500',
    fontSize: 14,
    color: '#979797',
    marginBottom: 10,
  },
  isiData: {
    color: '#201F26',
    fontSize: 14,
  },
  isiData1: {
    color: '#201F26',
    fontSize: 14,
    marginLeft: 9,
  },
  imageProduk: {
    width: 84,
    height: 84,
  },
  isiProduk: {
    width: '80%',
    marginLeft: 15,
    justifyContent: 'space-evenly',
  },
  reservasiButton: {
    width: '100%',
    backgroundColor: '#BB2427',
    borderRadius: 8,
    paddingVertical: 15,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
  },
  reservasiContainer: {
    marginRight: 20,
  },
  pasang: {
    color: '#000',
    fontSize: 16,
    fontWeight: '400',
    marginTop: 13,
  },
  isiRincian: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 11,
  },
  hargaPasang: {
    color: '#000',
    fontSize: 16,
    fontWeight: '700',
    marginRight: 22,
    marginTop: 13,
  },
  jenisLayanan: {
    color: '#000',
    fontSize: 12,
    fontWeight: '400',
  },
  jumlah: {
    color: '#FFC107',
    fontSize: 12,
    fontWeight: '500',
    marginLeft: 15,
  },
  hargaLayanan: {
    marginRight: 22,
    color: '#000',
    fontSize: 12,
    fontWeight: '400',
  },
  totalLayanan: {
    color: '#034262',
    marginRight: 22,
    fontSize: 12,
    fontWeight: '700',
  },
  lineStyle: {
    borderWidth: 1,
    borderColor: '#EDEDED',
    marginRight: 21,
    marginBottom: 8,
  },
  item: {
    backgroundColor: '#fff',
    marginRight: 12,
    borderWidth: 1,
    borderRadius: 3,
    borderColor: '#E1E1E1',
    width: 140,
    height: 90,
    justifyContent: 'center',
    alignItems: 'center',
  },
  kotakButtonPesan: {
    backgroundColor: '#fff',
    marginTop: 3,
    paddingVertical: 22,
    paddingLeft: 26,
  },
});

export default Checkout;
