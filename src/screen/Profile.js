import React, {useState} from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
} from 'react-native';

const Profile = ({navigation}) => {
  return (
    <View style={styles.all}>
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 50}}>
        <View>
          <View style={styles.kotakSummary}>
            <Image
              source={require('../assets/image/ProfPic.png')}
              style={styles.imagePP}
            />
            <Text style={styles.userName}>Agil Bani</Text>
            <Text style={styles.userEmail}>gilagil@gmail.com</Text>
            <TouchableOpacity
              onPress={() =>
                navigation.navigate('ProfileNavigation', {
                  screen: 'EditProfile',
                })
              }>
              <Text style={styles.edit}>Edit</Text>
            </TouchableOpacity>
          </View>
          <View style={{paddingHorizontal: 17}}>
            <View style={styles.kotak2}>
              <TouchableOpacity>
                <Text style={styles.isiProfile}>About</Text>
              </TouchableOpacity>
              <TouchableOpacity>
                <Text style={styles.isiProfile}>Terms & Condition</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() =>
                  navigation.navigate('ProfileNavigation', {screen: 'FAQ'})
                }>
                <Text style={styles.isiProfile}>FAQ</Text>
              </TouchableOpacity>
              <TouchableOpacity>
                <Text style={styles.isiProfile}>History</Text>
              </TouchableOpacity>
              <TouchableOpacity>
                <Text style={styles.isiProfile}>Settings</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={{paddingHorizontal: 17, marginTop: 19}}>
            <View style={styles.kotak2}>
              <View>
                <TouchableOpacity onPress={() => navigation.navigate('Login')}>
                  <View style={{flexDirection: 'row', paddingVertical: 14}}>
                    <Image
                      source={require('../assets/icon/logout.png')}
                      style={{width: 24, height: 24}}></Image>
                    <Text style={styles.logout}>Log out</Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  all: {
    backgroundColor: '#F6F8FF',
    flex: 1,
  },
  kotakSummary: {
    backgroundColor: '#fff',
    marginTop: 3,
    paddingTop: 52,
    marginBottom: 12,
    alignItems: 'center',
    paddingBottom: 40,
  },
  imagePP: {
    width: 95,
    height: 95,
  },
  userName: {
    color: '#050152',
    fontSize: 20,
    fontWeight: '700',
    marginTop: 6,
  },
  userEmail: {
    color: '#A8A8A8',
    fontWeight: '500',
    fontSize: 10,
    marginTop: 3,
  },
  edit: {
    color: '#050152',
    fontSize: 11,
    fontWeight: '600',
    backgroundColor: '#F6F8FF',
    paddingHorizontal: 21,
    paddingVertical: 7.83,
    borderRadius: 18,
    marginTop: 22,
  },
  kotak2: {
    backgroundColor: '#fff',
    alignItems: 'center',
  },
  isiProfile: {
    color: '#000',
    fontSize: 16,
    fontWeight: '500',
    marginVertical: 20,
  },
  logout: {
    color: '#EA3D3D',
    fontSize: 16,
    fontWeight: '500',
    marginLeft: 3,
  },
});

export default Profile;
