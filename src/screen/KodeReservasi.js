import React, {useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  ScrollView,
} from 'react-native';

const KodeReservasi = ({navigation}) => {
  return (
    <View style={styles.all}>
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 50}}>
        <View style={styles.listSummary}>
          <View style={styles.kotakSummary}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                fontSize: 12,
              }}>
              <Text style={{marginLeft: -26, color: '#BDBDBD', fontSize: 12}}>
                20 Desember 2020
              </Text>
              <Text style={{marginLeft: 11, color: '#BDBDBD', fontSize: 12}}>
                09:00
              </Text>
            </View>
            <Text style={styles.nomorKode}>CS122001</Text>
            <Text style={styles.kodeReservasi}>Kode Reservasi</Text>
            <Text style={styles.ketentuan}>
              Sebutkan Kode Reservasi saat tiba di outlet
            </Text>
          </View>
          <View style={{marginHorizontal: 11}}>
            <Text style={styles.titleKecil}>Barang</Text>
            <View style={styles.kotakSectionProduk}>
              <View
                style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                <Image
                  source={require('../assets/image/produk.png')}
                  style={styles.imageProduk}
                />
                <View style={styles.isiProduk}>
                  <Text style={{fontWeight: '500', color: '#000'}}>
                    New Balance - Pink Abu - 40
                  </Text>
                  <Text style={{color: '#737373'}}>Cuci Sepatu</Text>
                  <Text style={{color: '#737373'}}>Note : -</Text>
                </View>
              </View>
            </View>
            <Text style={styles.titleKecil}>Status Pesanan</Text>
            <TouchableOpacity
              style={styles.kotakSectionProduk}
              onPress={() =>
                navigation.navigate('HomeNavigation', {screen: 'Checkout'})
              }>
              <View
                style={{
                  flexDirection: 'row',
                  marginVertical: 23,
                  marginLeft: 18,
                }}>
                <View>
                  <Image
                    source={require('../assets/icon/pointmerah.png')}
                    style={{
                      width: 14,
                      height: 14,
                      marginTop: 3,
                      marginRight: 15,
                    }}></Image>
                </View>
                <View style={{width: '80%'}}>
                  <Text style={styles.telahReservasi}>Telah Reservasi</Text>
                  <Text style={styles.tanggalReservasi}>20 Desember 2020</Text>
                </View>
                <View>
                  <Text style={styles.tanggalReservasi}>09:00</Text>
                </View>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  all: {
    backgroundColor: '#F6F8FF',
    flex: 1,
  },
  back: {
    width: 24,
    height: 24,
    marginLeft: 21,
    marginTop: 19,
  },
  kotakSummary: {
    backgroundColor: '#fff',
    paddingVertical: 18,
    paddingLeft: 26,
    paddingBottom: 24,
    marginBottom: 12,
  },
  nomorKode: {
    color: '#201F26',
    fontWeight: '700',
    fontSize: 36,
    textAlign: 'center',
    marginLeft: -26,
    marginTop: 50,
  },
  kodeReservasi: {
    color: '#000',
    fontSize: 14,
    fontWeight: '400',
    textAlign: 'center',
    marginLeft: -26,
    marginTop: 5,
  },
  ketentuan: {
    color: '#6F6F6F',
    fontSize: 14,
    fontWeight: '400',
    textAlign: 'center',
    marginLeft: -26,
    marginTop: 42,
    width: 200,
    alignSelf: 'center',
  },
  kotakSectionProduk: {
    backgroundColor: '#fff',
    borderRadius: 9,
    marginTop: 9,
    elevation: 3,
  },
  imageProduk: {
    width: 84,
    height: 84,
    marginLeft: 14,
    marginVertical: 24,
  },
  isiProduk: {
    width: '80%',
    marginLeft: 15,
    justifyContent: 'space-evenly',
    paddingVertical: 23,
  },
  titleKecil: {
    color: '#201F26',
    fontSize: 14,
    fontWeight: '400',
    marginVertical: 10,
  },
  telahReservasi: {
    color: '#201F26',
    fontSize: 14,
    fontWeight: '400',
  },
  tanggalReservasi: {
    color: '#A5A5A5',
    fontSize: 10,
    fontWeight: '400',
    marginTop: 3,
  },
});

export default KodeReservasi;
