import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TextInput,
  ScrollView,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import {useSelector} from 'react-redux';

const Homepage = ({navigation}) => {
  const {toko} = useSelector(state => state.data);
  console.log(toko);

  return (
    <View style={styles.all}>
      <ScrollView
        nestedScrollEnabled={true}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 15}}>
        <View style={styles.section1}>
          <View style={styles.fitur}>
            <Image
              source={require('../assets/image/ProfPic.png')}
              style={{width: 45, height: 45}}
            />
            <TouchableOpacity
              onPress={() =>
                navigation.navigate('HomeNavigation', {screen: 'Keranjang'})
              }>
              <Image
                source={require('../assets/icon/Bag.png')}
                style={{width: 18, height: 20, marginTop: 10}}
              />
            </TouchableOpacity>
          </View>
          <Text style={styles.haloUser}>Hello, Agil!</Text>
          <Text style={styles.headerTitle}>
            Ingin merawat dan perbaiki sepatumu? Cari disini
          </Text>
          <View style={styles.SectionStyle1}>
            <View style={styles.SectionStyle}>
              <Image
                source={require('../assets/icon/Search.png')}
                style={styles.IconStyle}
              />
              <TextInput style={styles.search} />
            </View>
            <View style={styles.FilterStyle1}>
              <Image
                source={require('../assets/icon/Filter.png')}
                style={styles.FilterStyle}
              />
            </View>
          </View>
        </View>
        <View>
          <View style={styles.kotakMenu}>
            <View style={styles.kotakObjek}>
              <Image
                source={require('../assets/icon/sepatu.png')}
                style={styles.iconKotakStyle}
              />
              <Text style={styles.keteranganIcon}>Sepatu</Text>
            </View>
            <View style={styles.kotakObjek}>
              <Image
                source={require('../assets/icon/jaket.png')}
                style={styles.iconKotakStyle}
              />
              <Text style={styles.keteranganIcon}>Jaket</Text>
            </View>
            <View style={styles.kotakObjek}>
              <Image
                source={require('../assets/icon/tas.png')}
                style={styles.iconKotakStyle}
              />
              <Text style={styles.keteranganIcon}>Tas</Text>
            </View>
          </View>
        </View>
        <View style={styles.sectionRekomendasi}>
          <Text style={styles.rekomendasi}>Rekomendasi Terdekat</Text>
          <Text style={styles.viewAll}>View All</Text>
        </View>

        <FlatList
          nestedScrollEnabled={true}
          data={toko}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({item, index}) => (
            <TouchableOpacity
              style={styles.kotakSectionToko}
              onPress={() =>
                navigation.navigate('HomeNavigation', {
                  screen: 'Detail',
                  params: {item},
                })
              }>
              <View
                style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                <Image
                  source={require('../assets/image/jack1.png')}
                  style={styles.imageToko}
                />
                <View style={styles.isiToko}>
                  <Image
                    source={require('../assets/icon/ratings.png')}
                    style={{width: 53, height: 9}}
                  />
                  <Text
                    style={{
                      color: '#D8D8D8',
                      fontSize: 10,
                      paddingVertical: 5,
                    }}>
                    {item.ratings} Ratings
                  </Text>
                  <Text
                    style={{
                      color: 'black',
                      fontWeight: 'bold',
                      fontSize: 15,
                      paddingBottom: 5,
                    }}>
                    {item.storeName}
                  </Text>
                  <Text style={{fontSize: 9}}>{item.address}</Text>
                  <Text
                    style={
                      item.isOpen
                        ? styles.keteranganTokoBuka
                        : styles.keteranganTokoTutup
                    }>
                    {item.isOpen ? 'BUKA' : 'TUTUP'}
                  </Text>
                </View>
                <Image
                  source={require('../assets/icon/heart.png')}
                  style={{
                    width: 16,
                    height: 14,
                    marginTop: 15,
                    marginRight: 15,
                  }}
                />
              </View>
            </TouchableOpacity>
          )}
        />
      </ScrollView>
      <TouchableOpacity
        activeOpacity={0.8}
        style={styles.btnFloating}
        onPress={() =>
          navigation.navigate('HomeNavigation', {screen: 'AddDataHome'})
        }>
        <Icon name="plus" size={25} color="#fff" />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  btnFloating: {
    position: 'absolute',
    bottom: 30,
    right: 30,
    width: 40,
    height: 40,
    borderRadius: 20,
    backgroundColor: 'red',
    justifyContent: 'center',
    alignItems: 'center',
  },
  all: {
    backgroundColor: '#F6F8FF',
    flex: 1,
  },
  section1: {
    backgroundColor: '#fff',
    paddingTop: 56,
    paddingLeft: 22,
    paddingRight: 27,
  },
  fitur: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  haloUser: {
    paddingTop: 13,
    color: '#034262',
    fontSize: 15,
  },
  headerTitle: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: 23,
    paddingTop: 10,
  },
  search: {
    backgroundColor: '#F6F8FF',
    borderRadius: 14,
    width: '74%',
  },
  SectionStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#F6F8FF',
    height: 45,
    borderRadius: 14,
  },
  IconStyle: {
    height: 20,
    width: 20,
    marginLeft: 10,
    marginRight: 5,
  },
  FilterStyle: {
    height: 20,
    width: 20,
  },
  FilterStyle1: {
    backgroundColor: '#F6F8FF',
    padding: 13,
    borderRadius: 14,
  },
  SectionStyle1: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingBottom: 28,
    paddingTop: 20,
  },
  kotakMenu: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 18,
  },
  iconKotakStyle: {
    width: 45,
    height: 45,
  },
  kotakObjek: {
    backgroundColor: '#fff',
    marginHorizontal: 15,
    paddingHorizontal: 28,
    paddingVertical: 20,
    borderRadius: 16,
  },
  keteranganIcon: {
    textAlign: 'center',
    color: '#BB2427',
    marginTop: 9,
    fontSize: 10,
  },
  sectionRekomendasi: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: 20,
    marginTop: 27,
    marginBottom: 25,
  },
  rekomendasi: {
    color: '#0A0827',
    fontWeight: 'bold',
  },
  viewAll: {
    color: '#E64C3C',
    fontWeight: 'bold',
  },
  kotakSectionToko: {
    backgroundColor: '#fff',
    marginHorizontal: 20,
    borderRadius: 9,
    marginBottom: 5,
  },
  imageToko: {
    width: 100,
    height: 151,
    marginLeft: 6,
    marginVertical: 6,
  },
  isiToko: {
    marginTop: 15,
    width: '60%',
    marginLeft: 15,
  },
  keteranganTokoTutup: {
    backgroundColor: '#e64c3c33',
    color: '#EA3D3D',
    fontWeight: 'bold',
    borderRadius: 10.5,
    width: 65,
    padding: 3,
    textAlign: 'center',
    marginTop: 25,
  },
  keteranganTokoBuka: {
    backgroundColor: '#11a84e1f',
    color: '#11A84E',
    fontWeight: 'bold',
    borderRadius: 10.5,
    width: 65,
    padding: 3,
    textAlign: 'center',
    marginTop: 25,
  },
});

export default Homepage;
