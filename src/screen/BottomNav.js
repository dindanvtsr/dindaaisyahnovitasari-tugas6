import * as React from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import HomeNavigation from '../HomeNavigation';
import TransactionNavigation from '../TransactionNavigation';
import ProfileNavigation from '../ProfileNavigation';

function TabMenu({isFocused, label}) {
  const icon = iconType(label);
  return (
    <View
      style={{
        justifyContent: 'center',
        paddingBottom: 10,
        paddingTop: 15,
        backgroundColor: '#fff',
        borderTopLeftRadius: 14,
        borderTopRightRadius: 14,
      }}>
      <Image
        source={icon}
        style={{
          width: label === 'Transaction' ? 50 : 20,
          height: label === 'Transaction' ? 50 : 20,
          resizeMode: 'contain',
          tintColor: isFocused ? 'red' : 'grey',
          alignSelf: 'center',
        }}
      />
      <Text
        style={{
          color: isFocused ? 'red' : 'grey',
          marginTop: label === 'Transaction' ? -12 : 15,
          textAlign: 'center',
        }}>
        {label}
      </Text>
    </View>
  );
}

function iconType(label) {
  if (label === 'Home') {
    return require('../assets/icon/Home_ic.png');
  } else if (label === 'Transaction') {
    return require('../assets/icon/Transaction_ic.png');
  } else {
    return require('../assets/icon/Profile_ic.png');
  }
}

function MyTabBar({state, descriptors, navigation}) {
  return (
    <View style={{flexDirection: 'row'}}>
      {state.routes.map((route, index) => {
        const {options} = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;

        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
          });

          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate(route.name);
          }
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };

        return (
          <TouchableOpacity
            accessibilityRole="button"
            accessibilityState={isFocused ? {selected: true} : {}}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            onLongPress={onLongPress}
            style={{flex: 1}}>
            <TabMenu isFocused={isFocused} label={label} />
          </TouchableOpacity>
        );
      })}
    </View>
  );
}

const Tab = createBottomTabNavigator();

export default function BottomNav() {
  return (
    <Tab.Navigator
      screenOptions={{headerShown: false}}
      tabBar={props => <MyTabBar {...props} />}>
      <Tab.Screen name="Home" component={HomeNavigation} />
      <Tab.Screen name="Transaction" component={TransactionNavigation} />
      <Tab.Screen name="Profile" component={ProfileNavigation} />
    </Tab.Navigator>
  );
}
