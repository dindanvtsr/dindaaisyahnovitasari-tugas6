import React, {useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  ScrollView,
  Alert,
  Modal,
  Pressable,
} from 'react-native';

const DetailKeranjang = ({navigation}) => {
  const [modalVisible, setModalVisible] = useState(false);

  return (
    <View style={styles.all}>
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 50}}>
        <View style={styles.listSummary}>
          <View style={styles.kotakSummary}>
            <Text style={styles.dataTitle}>Data Customer</Text>
            <View style={{flexDirection: 'row'}}>
              <Text style={styles.isiData}>Agil Bani</Text>
              <Text style={styles.isiData1}>(0813763476)</Text>
            </View>
            <Text style={styles.isiData}>
              Jl. Perumnas, Condong catur, Sleman, Yogyakarta
            </Text>
            <Text style={styles.isiData}>gantengdoang@dipanggang.com</Text>
          </View>
          <View style={styles.kotakSummary}>
            <Text style={styles.dataTitle}>Alamat Outlet Tujuan</Text>
            <View style={{flexDirection: 'row'}}>
              <Text style={styles.isiData}>Jack Repair - Seturan</Text>
              <Text style={styles.isiData1}>(027-343457)</Text>
            </View>
            <Text style={styles.isiData}>
              Jl. Affandi No 18, Sleman, Yogyakarta
            </Text>
          </View>
          <View style={styles.kotakSummary}>
            <Text style={styles.dataTitle}>Barang</Text>
            <View
              style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <Image
                source={require('../assets/image/produk.png')}
                style={styles.imageProduk}
              />
              <View style={styles.isiProduk}>
                <Text style={{fontWeight: '500', color: '#000'}}>
                  New Balance - Pink Abu - 40
                </Text>
                <Text style={{color: '#737373'}}>Cuci Sepatu</Text>
                <Text style={{color: '#737373'}}>Note : -</Text>
              </View>
            </View>
          </View>
          <View style={styles.reservasiContainer}>
            <TouchableOpacity
              style={styles.reservasiButton}
              onPress={() => setModalVisible(true)}>
              <Text
                style={{
                  color: '#fff',
                  fontSize: 16.3,
                  fontWeight: 'bold',
                }}>
                Reservasi Sekarang
              </Text>
            </TouchableOpacity>
          </View>
          <View>
            <Modal
              animationType="slide"
              transparent={false}
              visible={modalVisible}
              onRequestClose={() => {
                Alert.alert('Modal has been closed.');
                setModalVisible(!modalVisible);
              }}>
              <View style={styles.header}>
                <TouchableOpacity
                  onPress={() =>
                    navigation.navigate('HomeNavigation', {
                      screen: 'Homepage',
                    })
                  }>
                  <Image
                    source={require('../assets/icon/close.png')}
                    style={styles.close}></Image>
                </TouchableOpacity>
              </View>
              <View style={styles.centeredView}>
                <View style={styles.modalView}>
                  <Text style={styles.modalTextBerhasil}>
                    Reservasi Berhasil
                  </Text>
                  <Image
                    source={require('../assets/icon/berhasil.png')}
                    style={styles.iconBerhasil}></Image>
                  <Text style={styles.keteranganModal}>
                    Kami Telah Mengirimkan Kode Reservasi ke Menu Transaksi
                  </Text>
                  <Pressable
                    style={[styles.button, styles.buttonClose]}
                    onPress={() => setModalVisible(!modalVisible)}>
                    <Text style={styles.textStyle}>Hide Modal</Text>
                  </Pressable>
                </View>
                <View style={{marginHorizontal: 20, marginBottom: 40}}>
                  <TouchableOpacity
                    style={styles.lihatButton}
                    onPress={() => navigation.navigate('Transaction')}>
                    <Text
                      style={{
                        color: '#fff',
                        fontSize: 16.3,
                        fontWeight: 'bold',
                      }}>
                      Lihat Kode Reservasi
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </Modal>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  all: {
    backgroundColor: '#F6F8FF',
    flex: 1,
  },
  header: {
    flexDirection: 'row',
    backgroundColor: '#fff',
  },
  back: {
    width: 24,
    height: 24,
    marginLeft: 21,
    marginTop: 19,
  },
  headerTitle: {
    color: '#201F26',
    fontWeight: 'bold',
    fontSize: 18,
    marginLeft: 10,
    marginTop: 17,
    marginBottom: 22,
  },
  kotakSummary: {
    backgroundColor: '#fff',
    marginTop: 3,
    paddingVertical: 18,
    paddingLeft: 26,
    marginBottom: 12,
  },
  dataTitle: {
    fontWeight: '500',
    fontSize: 14,
    color: '#979797',
    marginBottom: 10,
  },
  isiData: {
    color: '#201F26',
    fontSize: 14,
  },
  isiData1: {
    color: '#201F26',
    fontSize: 14,
    marginLeft: 9,
  },
  imageProduk: {
    width: 84,
    height: 84,
  },
  isiProduk: {
    width: '80%',
    marginLeft: 15,
    justifyContent: 'space-evenly',
  },
  reservasiButton: {
    width: '100%',
    backgroundColor: '#BB2427',
    borderRadius: 8,
    paddingVertical: 15,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '20%',
  },
  reservasiContainer: {
    marginHorizontal: 20,
  },
  centeredView: {
    flex: 1,
  },
  modalView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  modalTextBerhasil: {
    color: '#11A84E',
    fontWeight: '700',
    fontSize: 18,
  },
  iconBerhasil: {
    width: 160,
    height: 160,
    marginTop: 40,
    marginBottom: 55,
  },
  keteranganModal: {
    color: '#000',
    fontSize: 18,
    fontWeight: '400',
    textAlign: 'center',
    marginHorizontal: 50,
  },
  lihatButton: {
    width: '100%',
    backgroundColor: '#BB2427',
    borderRadius: 8,
    paddingVertical: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  close: {
    width: 24,
    height: 24,
    marginLeft: 21,
    marginTop: 19,
  },
});

export default DetailKeranjang;
