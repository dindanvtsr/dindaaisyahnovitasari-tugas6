import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  ScrollView,
  FlatList,
} from 'react-native';
import {useSelector} from 'react-redux';

const Keranjang = ({navigation}) => {
  const {cart} = useSelector(state => state.dataCart);
  console.log(cart);

  return (
    <View style={styles.all}>
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 50}}>
        <View style={styles.listKeranjang}>
          <FlatList
            nestedScrollEnabled={true}
            data={cart}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({item, index}) => (
              <TouchableOpacity
                style={styles.kotakSectionProduk}
                onPress={() =>
                  navigation.navigate('HomeNavigation', {
                    screen: 'DetailKeranjang',
                  })
                }>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}>
                  <Image
                    source={require('../assets/image/produk.png')}
                    style={styles.imageProduk}
                  />
                  <View style={styles.isiProduk}>
                    <Text style={{fontWeight: '500', color: '#000'}}>
                      {item.merk} - {item.color} - {item.size}
                    </Text>
                    <Text style={{color: '#737373'}}>Cuci Sepatu</Text>
                    <Text style={{color: '#737373'}}>{item.note}</Text>
                  </View>
                </View>
              </TouchableOpacity>
            )}
          />
          <View style={styles.sectionTambah}>
            <Image
              source={require('../assets/icon/Plus.png')}
              style={styles.iconTambah}></Image>
            <Text style={styles.textTambah}>Tambah Barang</Text>
          </View>
          <TouchableOpacity
            style={styles.selanjutnyaButton}
            onPress={() =>
              navigation.navigate('HomeNavigation', {screen: 'DetailKeranjang'})
            }>
            <Text
              style={{
                color: '#fff',
                fontSize: 16.3,
                fontWeight: 'bold',
              }}>
              Selanjutnya
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  all: {
    backgroundColor: '#F6F8FF',
    flex: 1,
  },
  back: {
    width: 24,
    height: 24,
    marginLeft: 21,
    marginTop: 19,
  },
  headerTitle: {
    color: '#201F26',
    fontWeight: 'bold',
    fontSize: 18,
    marginLeft: 10,
    marginTop: 17,
    marginBottom: 22,
  },
  listKeranjang: {
    marginHorizontal: 20,
  },
  selanjutnyaButton: {
    width: '100%',
    backgroundColor: '#BB2427',
    borderRadius: 8,
    paddingVertical: 15,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '75%',
  },
  kotakSectionProduk: {
    backgroundColor: '#fff',
    borderRadius: 9,
    marginBottom: 5,
    marginTop: 9,
    elevation: 3,
  },
  imageProduk: {
    width: 84,
    height: 84,
    marginLeft: 14,
    marginVertical: 24,
  },
  isiProduk: {
    width: '80%',
    marginLeft: 15,
    justifyContent: 'space-evenly',
    paddingVertical: 23,
  },
  sectionTambah: {
    flexDirection: 'row',
    marginTop: 40,
    justifyContent: 'center',
    alignItems: 'center',
  },
  iconTambah: {
    width: 20,
    height: 20,
    marginRight: 6,
  },
  textTambah: {
    fontWeight: '700',
    color: '#BB2427',
    fontSize: 14,
  },
});

export default Keranjang;
