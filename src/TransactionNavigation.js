import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import Transaction from './screen/Transaction';

const Stack = createNativeStackNavigator();

export default function TransactionNavigation() {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen
        name="TransactionPage"
        component={Transaction}
        options={{headerShown: true, headerTitle: 'Transaksi'}}
      />
    </Stack.Navigator>
  );
}
